

-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 13 Mars 2019 à 23:02
-- Version du serveur: 5.5.61-0ubuntu0.14.04.1-log
-- Version de PHP: 5.5.9-1ubuntu4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `web333_main`
--

-- --------------------------------------------------------

--
-- Structure de la table `Personne`
--

CREATE TABLE IF NOT EXISTS `Personne` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(15) NOT NULL,
  `prenom` varchar(15) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `email` varchar(150) NOT NULL,
  `date_inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `URL` varchar(150) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `Personne`
--

INSERT INTO `Personne` (`id`, `nom`, `prenom`, `sexe`, `email`, `date_inscription`, `URL`, `photo`) VALUES
(5, 'Valjean', 'Jean', 'M', 'wxcwx@gmail.ce', '2019-03-13 21:34:40', 'https://www.bonnenote.fr/svp?', 'images/491442df5f88c6aa018e86dac21d3606.jpg'),
(6, 'Ra', 'Caillou', 'F', 'azerwxcwx@gmail.nj', '2019-03-13 21:40:08', 'https://www.bonnenote.fr/svp?', 'images/250px-Racaillou-a-SL.png'),
(9, 'Dewavrin', 'Thibault', 'M', 'thib@mail.fr', '2019-03-13 21:59:19', 'https://www.bonnenote.fr/svp?', 'images/mowgli-personnage-livre-jungle-02.jpg'),
(10, 'Arias', 'Camila', 'F', 'ca@ari.co', '2019-03-13 22:00:30', 'https://www.bonnenote.fr/svp?', 'images/51uhYkSRV2L._SX466_.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
