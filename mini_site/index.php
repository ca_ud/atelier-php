<!doctype html> 
<html lang="fr">
<head>
<meta charset="utf-8">
<title>index.html </title>

<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php 

session_start();
if(!isset($_SESSION['counter'])) $_SESSION['counter'] = 0;
$_SESSION['counter']++;



?>
<body>
<div class="jumbotron ">
  <h1 class="display-4 text-center">Mini-site: ATE202!</h1>
  <p class="lead text-center">Nous sommes Thibault et Camila. <br> Dans ce mini-site vous pouvez gérer un Annuaire et voir les mesures.</p>
  <p class="text-monospace text-center">IMT Atlantique. 2019</p>
  <hr class="my-4">
  <div class="d-flex justify-content-center">
  <a class="btn btn-primary btn-lg " href= './admin.php'>accès administrateur</a> 
  </div>
  <hr class="my-4">
  <div class="d-flex justify-content-center">
	<a class="btn btn-info btn-lg " href= './utilisateur_liste.php'>accès utilisateur</a>
	</div>
	
</div>
	
</body>
</html>
 