<!doctype html> 
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Ajouter un utilisateur </title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/css?family=Libre+Barcode+39+Text|Special+Elite" rel="stylesheet"> 

</head>
<body>

	
<form method="post" name="ajouter" action="" enctype="multipart/form-data"> 
<div class="col-md-4">
	<label for="nom">Quel est votre nom ?</label>	
    <input type="text" class="form-control" name="nom" id="nom" size ="11.5" maxlength="30">
    <br>
    
    <label for="prenom">Quel est votre prénom ?</label>	
    <input type="text" class="form-control" name="prenom" id="prenom" size ="8.5" maxlength="15">
    <br>
    
    	<label for="sexe">Quel est votre sexe ?</label>	
   	 <br>
    
    <input type="radio" name="sexe" value="M" checked> M <br>
 	 <input type="radio" name="sexe" value="F"> F <br>
     <br> 
    
	 <label for="email">Quel est votre email ?</label>	
		<input type="email" class="form-control" name="email" id="email" >
		<br>
		
	<label for="URL">Quel est votre URL ?</label>	
		<input type="url" class="form-control" name="URL" id="URL" >
		


     <br>
     <input type="file" id="photo" class="form-control-file" name="photo" accept="image/png, image/jpeg">
     <input type="submit" value="Envoyer">
      <a class="btn btn-danger" href="./admin.php"> Retour en arrière </a>
	 <div>    
</form>

<a href="./admin.php"> Revenir en arrière </a>


<?php 
include_once("gestionBD.php");
if($_SERVER["REQUEST_METHOD"]== "POST" ){
	$pdo=connexion();
	$sexe = $_POST['sexe'];
	$nom = $_POST['nom'];
	$prenom = $_POST['prenom'];
	$email = $_POST['email'];
	$url = $_POST['URL'];

	//photo to server
	
	$folder="images/";
    $fileName = basename($_FILES['photo']['name']);
    $fileTmpName  = $_FILES['photo']['tmp_name'];
	 $uploadPath = $folder . $fileName; 

	if(move_uploaded_file($fileTmpName, $uploadPath)){
		echo "File". $fileName." upload";
	}else {
		echo "Erreur". $fileName;
	}

	
	
	
	if (strlen($nom)==0 || strlen($prenom) ==0
	||strlen($email)==0 || strlen($url) ==0 ){
	echo "<h1><font color='red'>Champs vide </font></h1> ";
	echo "<a href= './admin.php'>rediriger</a>";
	}else {
		$sql = "INSERT INTO Personne (id,nom,prenom,sexe, email, date_inscription,URL, photo) values(?, ?, ?, ? , ? , ? , ? , ?)";
		$q = $pdo->prepare($sql);
		$date = date('Y-m-d H:i:s');
      $q->execute(array(NULL,$nom,$prenom, $sexe, $email, $date,$url,$uploadPath));
      if($pdo){
		$pdo=NULL; // fermeture de la connexion 
		}
      header("Location: ./admin.php");
      exit; 
	}
	
		
	
}
?>


</body>
</html>
