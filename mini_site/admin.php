<!doctype html> 
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Ajouter un utilisateur </title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

 <link href="https://fonts.googleapis.com/css?family=Libre+Barcode+39+Text|Special+Elite" rel="stylesheet"> 
</head>

<body>
<a href="./ajouter.php" >Ajouter un utilisateur</a>

<form  method=\"post\">
	<table class="table">
	<thead>
	<tr>
	<th>ID</th>
	<th>Nom</th>
	<th>Prenom</th>
	<th>email</th>
	<th>date d'inscription</th>
	<th> url perso</th>
	<th> Edition</th>	
	<th> Supprimer</th>
	</tr>
	</thead>
	<tbody>
	<?php
	include_once("gestionBD.php");
	$pdo=connexion();
	$req="SELECT * FROM Personne";
	$res = $pdo->query($req);
	
	
	while($row=$res->fetch())
	echo "<tr>	
	<th name='id'>".$row['id']."</th>
	<th>".$row['nom']."</th>
	<th>".$row['prenom']."</th>
	<th>".$row['email']."</th>
	<th>".$row['date_inscription']."</th>
	<th>".$row['URL']."</th>
	<th><a class='btn btn-success' href=\"./modifier.php?id=" . $row['id'] . "\"> modifier </a></th>
	<th><a class='btn btn-danger' href=\"./supprimer.php?id=" . $row['id'] . "\"> supprimer </a></th>
	
	</tr> ";
	echo"</tbody>
	</table>
	</form>"; 
	if($pdo){
	$pdo=NULL; // fermeture de la connexion 
	}
	
	?>
<?php include("statistiques.php");
?>


<a href="./index.php" > retour à l'accueil </a>
</body>
</html>
