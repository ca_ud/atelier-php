

<?php
    $currentDir = getcwd();
    $uploadDirectory = "/uploads/";

    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 

    if (isset($_POST['submit'])) {
        if ($fileSize > 2000000) {
           echo "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }else {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
            if ($didUpload) {
                echo "The file " . basename($fileName) . " has been uploaded";
            } else {
                echo "An error occurred somewhere. Try again or contact the admin";
            }
        } 
    }


?>


